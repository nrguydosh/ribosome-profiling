import argparse
import rfanalyze as rfa
import rfquant as rfq
import rfposavg as rfp

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('mode')
    parser.add_argument('config')
    args = parser.parse_args()

    if args.mode == "rfanalyze":
        rfa.rfanalyze_workflow(args.config)
    elif args.mode == "rfquant":
        rfq.quant_workflow(args.config)
    elif args.mode == "rfposavg":
        rfp.posavg_workflow(args.config)
    else:
        print "Did not recognize mode."

if __name__ == '__main__':
    main()
    
