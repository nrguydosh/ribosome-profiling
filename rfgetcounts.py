from Bio.Seq import Seq

class SeqCountGetter(object):
    """ SeqCountGetter wraps givegene in a class. It calculates the counts
        and seq for a particular feature with some shift. It also optionally checks 
        for dubious features that overlap and badly formed gff gene entries.   
    """
    def __init__(self, counts, coding, utr5=None, utr3=None, check_overlap=True):
        self.counts = counts
        self.coding = coding
        self.utr5 = {} if not utr5 else utr5
        self.utr3 = {} if not utr3 else utr3
        self.check_overlap = check_overlap
    
    def get_counts(self, chromosome, feature, shift):
        try:
            bp = self.__check_shift(shift)
        except ValueError:
            return [-1, -1]

        # Define strand and shift
        strand = self.coding[chromosome].features[feature].strand
        if strand == -1:
            bp = [bp[1], bp[0], bp[2]]
        
        # Define start and end of actual feature
        start=self.coding[chromosome].features[feature].location.start.position
        end=self.coding[chromosome].features[feature].location.end.position
        
        # Check for overlap
        if self.check_overlap:
            if self.__is_overlap(chromosome, feature, start, end, strand, bp):
                return [-2, -2]
       
        # Splice the CDS
        splicedseq, splicedcounts = self.__splice_cds(chromosome, feature, strand)
        if splicedcounts==[] or str(splicedseq)=='':
            return [-1,-1]
        
        # Add upstream/downstream 
        splicedseq, splicedcounts = self.__add_front_and_back(strand, chromosome, splicedcounts, splicedseq, start, end, bp)
        # Shift counts
        splicedcounts = self.__shift_counts(chromosome, splicedcounts, bp, start, end, strand)

        # Reverse if on negative strand
        if strand==-1:
            splicedcounts.reverse()
            splicedseq=splicedseq.reverse_complement()
        
        return [splicedcounts,splicedseq]

    def __check_shift(self, bp):
        """ Checks the shift bp parameter
            If bp is an int, then return [bp, bp, 0]
            If bp is a list of length 2 then return [bp[0], bp[1], 0]
            Else return [bp[0], bp[1], bp[2]]
            Where the list of shift is [5' upstream, 3' downstream, ribosome shift]
            If bp provided is negative, raiseValueError
        """
        if type(bp) != int:
            if len(bp)==2:
                bp.append(0)
        else:
            bp = [bp, bp, 0]
        if bp[0] < 0 or bp[1] < 0:
            print "Error, bp is negative!"
            raise ValueError 
        return bp
    
    def __get_neighbors(self, chromosome,feature):
        """ Returns feature numbers of nearest genes.
        If no neighbor (i.e. at the end of the gene), returns -1
        """
        if feature<0 or feature >=len(self.coding[chromosome].features):
            print "Illegal feature given to neigbors program."
            return[-1,-1]
        
        if (feature-1)<0:
            prevfeat = -1
        else: 
            prevfeat = feature-1
        if (feature+1)>=len(self.coding[chromosome].features):
            nextfeat = -1
        else:
            nextfeat = feature+1

        return [prevfeat,nextfeat]


    def __is_overlap(self, chromosome, feature, start, end, strand, bp):
        """ Check if there is an overlap of genes going in the same direction
            If self.utr5 and self.utr3 are not empty dictionaries, check will include
            utr regions. Riboshift is not included since it should affect all genes
            equally. 
        """
        
        # get the ending position and direction of the previous feature and the
        # starting position and direction of the next feature
        ### Shouldn't get_neighbors take care of this? And shouldn't get_neighbors keep 
        ### looking for a neighboring feature in the same direction rather than
        ### returning the neighbors that are non-dubious?
        feats=self.__get_neighbors(chromosome,feature)
        prevfeat=feats[0]
        nextfeat=feats[1]
        if prevfeat != -1:
            prevend=self.coding[chromosome].features[prevfeat].location.end.position
            prevdir=self.coding[chromosome].features[prevfeat].strand==self.coding[chromosome].features[feature].strand
        else:
            prevend = len(self.coding[chromosome].seq)
            prevdir = False
        if nextfeat != -1:
            nextstart=self.coding[chromosome].features[nextfeat].location.start.position
            nextdir=self.coding[chromosome].features[nextfeat].strand==self.coding[chromosome].features[feature].strand
        else:
            nextstart = 0
            nextdir = False
       
        # Add on the 5' and 3' but first calculate the length of the UTRs instead of
        # using given positions
        # Add 5' to the front and 3' to the end on the positive strand
        # Add 3' to the front and 5' to the end on the negative strand
        prevfeatid=self.coding[chromosome].features[prevfeat].id
        nextfeatid=self.coding[chromosome].features[nextfeat].id
        if self.utr5.has_key(prevfeatid):
            prev5=self.utr5[prevfeatid][1]-self.utr5[prevfeatid][0]
        else:
            prev5=0
        if self.utr3.has_key(prevfeatid):
            prev3=self.utr3[prevfeatid][1]-self.utr3[prevfeatid][0]
        else:
            prev3=0

        if self.utr5.has_key(nextfeatid):
            next5=self.utr5[nextfeatid][1]-self.utr5[nextfeatid][0]
        else:
            next5=0
        if self.utr3.has_key(nextfeatid):
            next3=self.utr3[nextfeatid][1]-self.utr3[nextfeatid][0]
        else:
            next3=0

        if strand==1:
            if prevdir==True:
                prevend+=prev3
            if nextdir==True:
                nextstart-=next5
        else:
            if prevdir==True:
                prevend+=prev5
            if nextdir==True:
                nextstart-=next3
        
        # Check if overlapping
        if (start-bp[0])<prevend and prevdir==True:
            return True
        if (end+bp[1])>nextstart and nextdir==True:
            return True

        return False

    def __splice_cds(self, chromosome, feature, strand):
        """ Assemble sequence and counts by splicing out introns
        """
        splicedseq=Seq('')
        splicedcounts=[]
        for item in self.coding[chromosome].features[feature].sub_features:
            if item.type == 'CDS':
                start_feat = int(item.location.start.position)
                end_feat = int(item.location.end.position)
                splicedseq+=(self.coding[chromosome][start_feat:end_feat]).seq
                
                splicedcounts+=self.counts[strand][chromosome][start_feat:end_feat]
        return (splicedseq, splicedcounts)

    def __add_front_and_back(self, strand, chromosome, splicedcounts, splicedseq, start, end, bp):
        """ Add on extra bit on front and back.
        """
        splicedcounts=self.counts[strand][chromosome][start-bp[0]:start]+splicedcounts
        splicedseq=self.coding[chromosome][start-bp[0]:start].seq+splicedseq
        splicedcounts+=self.counts[strand][chromosome][end:end+bp[1]]
        splicedseq+=self.coding[chromosome][end:end+bp[1]].seq
        return (splicedseq, splicedcounts)

    def __shift_counts(self, chromosome, splicedcounts, bp, start, end, strand):
        """ Perform ribosome shift for counts.
        """
        if strand==1:
            splicedcounts=self.counts[strand][chromosome][start-bp[0]-bp[2]:start-bp[0]]+splicedcounts[:-bp[2]]
        else:
            splicedcounts=splicedcounts[bp[2]:]+self.counts[strand][chromosome][end+bp[1]:end+bp[1]+bp[2]]
        return splicedcounts
