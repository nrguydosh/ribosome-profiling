import argparse
from rfgetcounts import SeqCountGetter
from featrecord import FeatureRecord
from featrecord import FeatureRecordHolder
import matplotlib.pyplot as plt
import rfhelper as rfh
import struct

class PositionAverager(object):
    """ PositionAverager averages counts at a given position. It requires
        a FeatureRecordHolder. 
    """

    def __init__(self, holder, utr5ext=0, utr3ext=0, coding5ext=0, coding3ext=0):
        self.holder = holder
        self.utr5ext = utr5ext
        self.utr3ext = utr3ext
        self.coding5ext = coding5ext
        self.coding3ext = coding3ext
        self.avgs = None
        self.count = None
    
    def __get_utr_length(self, utrtable, featureid):
        """ Gets the length of utr given utrtable and featureid
        """
        if utrtable.has_key(featureid):
            return utrtable[featureid][1] - utrtable[featureid][0]
        else:
            return 0

    def get_counts(self, counts, riboshift, coding, utr5=None, utr3=None, 
                   overlap=False):
        
        scgetter = SeqCountGetter(counts, coding, utr5, utr3, overlap)

        for chrom in coding:

            feat_num = -1

            for feature in coding[chrom].features:

                feat_num += 1

                # for efficiency
                if feature.id not in self.holder.records.keys():
                    continue

                shift = [0, 0, riboshift]
                utr5_length = self.__get_utr_length(utr5, feature.id)
                if utr5_length:
                    utr5_length += self.utr5ext
                utr3_length = self.__get_utr_length(utr3, feature.id) + self.utr3ext
                if utr3_length:
                    utr3_length += self.utr3ext
                full_length = (feature.location.end.position - 
                               feature.location.start.position)
                
                shift[0] += max(utr5_length, self.coding5ext)
                shift[1] += max(utr3_length, self.coding3ext)

                gene_counts, gene_seqs = scgetter.get_counts(chrom, feat_num, shift)
                if gene_counts < 0:
                    continue
               
                coding_length = len(gene_counts) - utr5_length - utr3_length
                
                # don't need to consider strand because get_counts reverses if -1
                
                if utr5_length >= self.coding5ext:
                    utr5_counts = gene_counts[:utr5_length] 
                    # self.utr5ext gives utr5 '0' position
                    coding_start = utr5_length - self.coding5ext 
                    # self.coding5ext gives gives coding '0' position
                    extend_safe5 = True
                else:
                    utr5_counts = gene_counts[self.coding5ext-utr5_length:self.coding5ext] 
                    # self.utr5ext gives utr5 '0' position
                    coding_start = 0 
                    # self.coding5ext gives coding '0' position
                    extend_safe5 = False

                coding_counts = gene_counts[coding_start:
                    self.coding5ext+coding_start+full_length+self.coding3ext]        
                
                utr3_counts = gene_counts[coding_start+self.coding5ext+full_length:
                    coding_start+self.coding5ext+full_length+utr3_length] 
                # utr3 '0' is always 0

                if utr3_length >= self.coding3ext:
                    extend_safe3 = True
                else:
                    extend_safe3 = False

                self.holder.records[feature.id].set_hidden({"utr5_counts":utr5_counts,
                    "coding_counts":coding_counts, "utr3_counts":utr3_counts, 
                    "extend_safe5":extend_safe5, "warning3":extend_safe3})


    def plot_position_average(self, addl_header, region, window, 
                              weighted=False, ignore_utr = True):

        assert region in ["utr5", "coding", "utr3"]
        region_counts = region+"_counts"
        all_average = [0 for i in range(window[0] + window[1])]
        noOfPositions = 0
        for record in self.holder.records.values():
            if region_counts not in record.hidden.keys():
                continue
            if region == "utr3":
                if not ignore_utr:
                    if not extend_safe3:
                        continue
                positions = [(int(pos)-window[0], int(pos)+window[1])
                             for pos in record.addl[addl_header]]
            elif region == "utr5":
                if not ignore_utr:
                    if not extend_safe5:
                        continue
                positions = [(int(pos)+self.utr5ext-window[0], 
                              int(pos)+self.utr5ext+window[1])
                             for pos in record.addl[addl_header]]
            else:
                # ignore utr?
                positions = [(int(pos)+self.coding5ext-window[0], 
                              int(pos)+self.coding5ext+window[1])
                             for pos in record.addl[addl_header]]
            for start, end in positions:
                if start < 0:
                    continue
                if end > len(record.hidden[region_counts]):
                    continue
                loc_counts = record.hidden[region_counts][start:end]
                total_loc_counts = 1 if weighted else sum(loc_counts)
                if total_loc_counts > 0:
                    all_average = [all_average[i] + loc_counts[i]/total_loc_counts for 
                                   i in range(len(all_average))] 
                    noOfPositions += 1 
        
        if noOfPositions == 0:
            print "No genes available."
            return
       
        print "There are", noOfPositions, "genes."

        for i in range(len(all_average)):
            all_average[i] = all_average[i]/noOfPositions

        self.avgs = [all_average]
        self.count = noOfPositions

        xaxis = range(-window[0],window[1])
        plt.plot(xaxis, all_average)
        plt.xlabel('Position in ' + region)
        plt.ylabel('Averaged local counts')
        plt.show()

    def write_binary(self, outfile):
        if self.avgs:
            out = open(outfile, 'wb')
            for i in range(len(self.avgs[0])):
                out.write(struct.pack("f", float(self.avgs[0][i])))
            out.close()
        else:
            print "did not write output."

def parse_rfp_config(config):
    configd = {}
    for line in open(config):
        l = line.strip().split(': ')
        if l[1] == "":
            continue
        configd[l[0]] = l[1]
    configd['cleancodingGFF'] = True if configd['cleancodingGFF'] == "True" else False
    configd['checkoverlap'] = True if configd['checkoverlap'] == "True" else False
    configd['riboshift'] = int(configd['riboshift'])
    configd['window5'] = int(configd['window5'])
    configd['window3'] = int(configd['window3'])
    configd['utr3ext'] = int(configd['utr3ext'])
    configd['utr5ext'] = int(configd['utr5ext'])
    configd['coding5ext'] = int(configd['utr5ext'])
    configd['coding3ext'] = int(configd['utr3ext'])
    configd['ignore_utr'] = True if configd['ignore_utr'] == "True" else False
    configd['weighted'] = True if configd['weighted'] == "True" else False
    return configd    

def posavg_workflow(config):
    
    configd = parse_rfp_config(config)
    
    countsp = rfh.wigtocounts(configd['countsfilestring']+"_plus.wig")
    countsm = rfh.wigtocounts(configd['countsfilestring']+"_minus.wig")
    counts = {1:countsp, -1:countsm}
    
    # parsing gff
    print "parsing gff..."
    coding = rfh.makeGFFlist(configd['codingGFF'])
    if configd['cleancodingGFF']:
        coding = rfh.clean_feature_dict(coding)
    utr5 = rfh.parse_GFF(configd['utr5GFF']) if configd['utr5GFF'] else {}
    utr3 = rfh.parse_GFF(configd['utr3GFF']) if configd['utr3GFF'] else {}

    holder = FeatureRecordHolder()
    holder.read_csv(configd['csv'])

    pa = PositionAverager(holder, configd['utr5ext'], configd['utr3ext'],
                         configd['coding5ext'], configd['coding3ext'])
    pa.get_counts(counts, configd['riboshift'], coding, utr5, utr3, configd['checkoverlap'])
    pa.plot_position_average('mrna_pos', 'coding', [configd['window5'], configd['window3']],
                             configd['weighted'], configd['ignore_utr'])
    pa.write_binary(configd['outbin'])
                          
def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('codingGFF', help = 'coding GFF')
    parser.add_argument('countsfilestring', help = 'input counts string')
    parser.add_argument('csv', help = 'input csv')
    parser.add_argument('--riboshift', default = 13, help = 'ribosome shift')
    parser.add_argument('--win5', default = 100, help = 'window toward the left')
    parser.add_argument('--win3', default = 100, help = 'window toward the right')
    parser.add_argument('--utr5GFF', help = 'utr5 GFF')
    parser.add_argument('--utr3GFF', help = 'utr3 GFF')
    parser.add_argument('--utr5ext', default = 0, 
                        help = "no. of bases to extend 5' UTR by")
    parser.add_argument('--utr3ext', default = 0, 
                        help = "no. of bases to extend 3' UTR by")
    parser.add_argument('--coding5ext', default = 0, 
                        help = "no. of bases to extend coding in the 5' direction")
    parser.add_argument('--coding3ext', default = 0, 
                        help = "no. of bases to extend coding in the 3' direction")
    parser.add_argument('--check_overlap', default = False)
    parser.add_argument('--weighted', default = False)
    parser.add_argument('--ignore_utr', default = True)
    parser.add_argument('--outputfile', default = "out.bin",
                        help = "binary file for igor")

    args = parser.parse_args()

    # reading counts
    print "reading counts..."
    #countsp = rfh.readcountsf(args.countsfilestring+"_plus_")
    #countsm = rfh.readcountsf(args.countsfilestring+"_minus_")
    countsp = rfh.wigtocounts(args.countsfilestring+"_plus.wig")
    countsm = rfh.wigtocounts(args.countsfilestring+"_minus.wig")
    counts = {1:countsp, -1:countsm}
    
    # parsing gffs
    print "parsing gff..."
    coding = rfh.clean_feature_dict(rfh.makeGFFlist(args.codingGFF))
    if args.utr5GFF:
        utr5 = rfh.parse_GFF(args.utr5GFF)
    else:
        utr5 = {}
    if args.utr3GFF:
        utr3 = rfh.parse_GFF(args.utr3GFF)
    else:
        utr3 = {}

    holder = FeatureRecordHolder()
    holder.read_csv(args.csv)

    pa = PositionAverager(holder, int(args.utr5ext), int(args.utr3ext), 
                          int(args.coding5ext), int(args.coding3ext))
    pa.get_counts(counts, int(args.riboshift), coding, utr5, utr3, args.check_overlap)
    pa.plot_position_average('mrna_pos', 'coding', [int(args.win5), int(args.win3)],
                             args.weighted, args.ignore_utr)
    pa.write_binary(args.outputfile)


if __name__ == '__main__':
    main()
