class FeatureRecord(object):
    """ FeatureRecord holds all information needed to write csv line(s) 
        corresponding to that feature to a file. The FeatureRecord object 
        must have a feature id, alias, and chromosome. seq defaults to 
        and note both default to the string "<none>". addl refers 
        to any additional information that the FeatureRecord should hold 
        and is a dictionary, addl[header] = [value1, value2, value3]. hidden 
        refers to any other information that the FeatureRecord should 
        hold and will not be written to the csv file.

        >>> addl = {"header1":["value1","value2"]}
        >>> record = FeatureRecord("id", "<n/a>", "chrom", addl=addl)
        >>> print record.to_csv_line(["header1"])
        id,<n/a>,chrom,value1,<none>,<none>
        id,<n/a>,chrom,value2,<none>,<none>

    """

    def __init__(self, fid, alias, chrom, seq="<none>", note="<none>", 
                 addl=None, hidden=None):
        self.fid = fid
        self.alias = alias
        self.chrom = chrom
        self.seq = seq
        self.note = note
        self.addl = {} if not addl else addl
        self.hidden = {} if not hidden else hidden

    def get_addl_headers(self):
        """ Get additional headers.
        """
        return self.addl.keys()

    def set_addl(self, addl_to_add):
        """ Set in the additional dictionary.
        """
        self.addl = addl_to_add

    def set_hidden(self, hidden):
        """ Set hidden
        """
        self.hidden = hidden

    def add_addl(self, addl_to_add):
        pass

    def add_hidden(self, hidden):
        pass

    def remove_duplicates(self, addl_headers):
        """ Remove all duplicates in addl given a set of headers
        """ 
        if len(self.addl.keys()) == 0:
            return # addl dictionary doesn't have keys yet
        for addl_header in addl_headers: # those headers given exist
            assert addl_header in self.addl.keys()
        if (len(addl_headers)) == 0:
            return
        no_of_lines = len(self.addl.values()[0])
        if no_of_lines == 0:
            return # addl dictionary exists, but is empty. that's fine
        if no_of_lines == 1:
            return # can't be a duplicate if there's just one
        lines = []
        unique = []
        for i in range(no_of_lines):
            line = (["{0!s}".format(self.addl[addl_header][i]) for
                        addl_header in addl_headers])
            line_to_string = ",".join(line)
            if line_to_string not in lines:
                unique.append(i)
                lines.append(line_to_string)
        new_addl = {}
        for header in addl_headers:
            new_addl[header] = [self.addl[header][i] for i in range(no_of_lines)
                                if i in unique]
        self.addl = new_addl

    def to_csv_line(self, csv_addl_header=None):
        """ Formats to a csv line with given csv additionl headers.
            csv is comma delimited, formatted
            fid, alias, chrom, ... , seq, note
            ... are those values given in the addl dictionary keyed
            by the given csv_addl_header. 
        """
        if csv_addl_header:
            for addl_header in csv_addl_header:
                assert addl_header in self.addl.keys()
        if len(self.addl.keys()) == 0: # no keys in the addl
            line = [self.fid, self.alias, self.chrom, str(self.seq), self.note]
            return ",".join(line)
        no_of_lines = len(self.addl.values()[0])
        if no_of_lines == 0: # no values in the addl that are not empty
            line = [self.fid, self.alias, self.chrom]
            if csv_addl_header != None:
                line.extend(["-1" for i in range(len(csv_addl_header))])
            line.extend([str(self.seq), self.note])
            line_to_string = ",".join(line)
            return line_to_string
        lines = set([]) 
        for i in range(no_of_lines):
            line = [self.fid, self.alias, self.chrom] 
            if csv_addl_header != None:
                line.extend(["{0!s}".format(self.addl[addl_header][i]) for 
                         addl_header in csv_addl_header])
            line.extend([str(self.seq), self.note])
            line_to_string = ",".join(line)
            lines.add(line_to_string)
        tostring = "\n".join(lines)
        return tostring

class FeatureRecordHolder(object):
    """ FeatureRecordHolder holds a list of FeatureRecords, and
        guarantees that the addl_headers for all FeatureRecords in the list 
        are the same. It can both read and write a csv file. It can also
        filter FeatureRecords by feature ids and by some basic condition
        on the addl dictionary, returning a new FeatureRecordHolder. 

        >>> addl = {"header1":["value1","value2"]}
        >>> record1 = FeatureRecord("id", "<n/a>", "chrom", addl=addl)
        >>> addl = {"header1":["value3"]}
        >>> record2 = FeatureRecord("id", "<n/a>", "chrom", addl=addl)
        >>> holder = FeatureRecordHolder(["header1"])
        >>> holder.add_record(record1)
        >>> holder.add_record(record2)
        >>> record3 = FeatureRecord("id2", "<n/a>", "chrom", addl=addl)
        >>> holder.add_record(record3)
        >>> print holder.records["id"].to_csv_line(["header1"])
        id,<n/a>,chrom,value1,<none>,<none>
        id,<n/a>,chrom,value2,<none>,<none>
        id,<n/a>,chrom,value3,<none>,<none>
        >>> print holder.records["id2"].to_csv_line(["header1"])
        id2,<n/a>,chrom,value3,<none>,<none>
    """

    def __init__(self, addl_headers=None, records=None):
        self.addl_headers = [] if not addl_headers else addl_headers
        self.records = {} 
        if records:
            for record in records:
                self.add_record(record)
        self.out_handle = None

    def add_record(self, record):
        """ Add a record to the featurerecordholder 
        """
        record_addl_headers = record.get_addl_headers()
        for addl_header in self.addl_headers: # make sure that all required headers
                                              # are available
            assert addl_header in record_addl_headers
        if record.fid in self.records.keys():
            assert str(self.records[record.fid].seq) == str(record.seq)
            for header in self.addl_headers:
                self.records[record.fid].addl[header].extend(record.addl[header])
        else:
            self.records[record.fid] = record
        self.records[record.fid].remove_duplicates(self.addl_headers) # works, but lazy
                                                                      # should iterate
                                                                      # through and check

    def get_record_list(self):
        """ Get the list of records.
        """
        return self.records.values()

    def add_headers(self, header=None, headers=None):
        """ Add headers to addl_headers.
        """
        assert ((header == None and headers != None) or 
                (header != None and headers == None))
        if header != None:
            self.addl_headers.append(header)
        else:
            self.addl_headers.extend(headers)

    def set_headers(self, addl_headers):
        """ Set addl_headers (overwrites)
        """
        self.addl_headers = addl_headers

    def add_comment_values(self, comment, addl_values):
        """ Adds comments to a csv
            Up to the user to make sure that the length is
            correct
            Future: Use out_headers instead
        """
        assert self.out_handle != None
        comment = ["#"+comment, "NA", "NA"]
        comment.extend(["{0:.6f}".format(addl_values[addl_header]) 
                        for addl_header in self.addl_headers])
        comment.extend(["NA", "NA"])
        self.out_handle.write(",".join(comment)+"\n")

    def write_to_csv(self, out_handle, out_headers=None):
        """ Write to a csv file.
        """
        self.out_handle = out_handle
        if not out_headers: # flexible headers for the csv
            out_headers = self.addl_headers
        header = ["feature.id", "alias", "chrom"]
        header.extend(out_headers)
        header.extend(["seq", "note"])
        header = ",".join(header)
        self.out_handle.write(header+"\n")
        for record in self.get_record_list():
            self.out_handle.write(record.to_csv_line(out_headers)+"\n")

    def close_csv(self):
        # used with writing, to allow adding extra lines as comments
        self.out_handle.close()

    def read_csv(self, csv):
        """ Read a csv 
        """
        csv_handle = open(csv)
        self.addl_headers = csv_handle.readline().strip().split(',')[3:-2]
        for line in csv_handle:
            l = line.strip().split(',')
            fid = l[0]
            alias = l[1]
            chrom = l[2]
            seq = l[-2]
            note = l[-1]
            addl = {}
            for headeri in range(len(self.addl_headers)):
                try:
                    value = float(l[3+headeri])
                except ValueError:
                    value = l[3+headeri]
                addl[self.addl_headers[headeri]] = [value]
            new_record = FeatureRecord(fid, alias, chrom, seq, note, addl)
            self.add_record(new_record)

    def filter_by_featids(self, featids):
        """ Filters by list of given feature ids
        """
        new_holder = FeatureRecordHolder(addl_headers = self.addl_headers,
                     records = [self.records[featid] for featid in featids])
        return new_holder
       
    def filter_by_addl(self, header, comparator, value):
        """ Filters by additional headers
            Header is the header you're filtering by
            Comparator is -1 for <, 0 for ==, 1 for >
            Where you are comparing record.addl[header][i] comparator value
            so for e.g. "coding_rpkm", 1, 0 filters all records which have coding_rpkm > 0
        """
        new_holder = FeatureRecordHolder(addl_headers = self.addl_headers)
        for record in self.records.values():
            no_of_lines = len(record.addl.values()[0])
            addl = dict([(header, []) for header in self.addl_headers])
            for i in range(no_of_lines):
                if ( (record.addl[header][i] > value) - (record.addl[header][i] < value) 
                    == comparator):
                    for header in self.addl_headers:
                        addl[header].append(record.addl[header][i])
            new_record = FeatureRecord(record.fid, record.alias, record.chrom,
                                       record.seq, record.note, addl)
            new_record.remove_duplicates(self.addl_headers)
            new_holder.add_record(new_record)
        return new_holder
