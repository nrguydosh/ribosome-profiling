import argparse
import rfhelper as rfh
from rfgetcounts import SeqCountGetter
from featrecord import FeatureRecord
from featrecord import FeatureRecordHolder
from itertools import islice
import pdb

class FeatureQuantifier(object):
    """ FeatureQuantifier quantifies a feature. It needs a coding counts dictionary, 
        and a coding dictionary, as well as optionally, a 5' utr and 3' utr dictionary. 
        It also takes a ribosome_shift. It's main methods right now are to calculate
        rpkm for the coding, 5' utr and 3' utr regions. It saves the information
        as a FeatureRecord, saving also the counts corresponding to each region
        in the hidden dictionary for future processing. 
    """

    def __init__(self, counts, ribosome_shift, coding, utr5=None, utr3=None, 
                 utr5extend = 0, utr3extend = 0):
        self.coding = coding
        self.utr5 = {} if not utr5 else utr5
        self.utr3 = {} if not utr3 else utr3
        self.counts = counts
        self.ribosome_shift = ribosome_shift
        self.holder = None
        self.utr5extend = utr5extend
        self.utr3extend = utr3extend
        
    def __get_utr_length(self, utrtable, featureid):
        """ Gets the length of utr given utrtable and featureid
        """
        if utrtable.has_key(featureid):
            return utrtable[featureid][1] - utrtable[featureid][0]
        else:
            return 0

    def __calculate_rpkm(self, counts):
        """ Calculate rpkm given list of counts.
        """
        if len(counts) == 0:
            return 0
        else:
            return sum(counts)*1000/len(counts)

    def __window(self, seq, n):
        it = iter(seq)
        result = tuple(islice(it, n))
        if len(result) == n:
            yield result
        for elem in it:
            result = result[1:] + (elem,)
            yield result

    def __get_utr3_peaks(self, utr3counts):
        """ Get the peak position in the utr3 position
            Smooth 3' UTR using 3bp boxcar
            Future: make boxcar variable
        """
        if len(utr3counts) < 3: 
            return [-1]
        utr3counts_smooth = [sum(w)/float(len(w)) for w in 
                             self.__window(utr3counts, 3)]
        utr3counts_smooth = [0] + utr3counts_smooth + [0]
        assert len(utr3counts) == len(utr3counts_smooth)
        utr3_peaks = [i for i, j in enumerate(utr3counts_smooth) if 
                      (abs(j-max(utr3counts_smooth)) < 1e-12)]
        if len(utr3_peaks) == len(utr3counts):
            return [-1]
        else:
            return utr3_peaks
 
    def quantify(self):
        """ Main quantification loop
            Future: organize information as a class, separate from IO
        """

        coding = self.coding
        utr5table = self.utr5
        utr3table = self.utr3
        counts = self.counts
        riboshift = self.ribosome_shift

        scgetter = SeqCountGetter(self.counts, self.coding, self.utr5, self.utr3, True)
        holder = FeatureRecordHolder(addl_headers =
                                     ["utr5_rpkm", "coding_rpkm", "utr3_rpkm",
                                      "utr5_length", "coding_length", "utr3_length",
                                      "utr5_seq", "coding_seq", "utr3_seq"])

        for chrom in coding:
            
            feat_num = -1
            
            for feature in coding[chrom].features:

                feat_num += 1
                strand = feature.strand
                alias = rfh.qualifier_to_string(feature, 'Alias')
                note = rfh.qualifier_to_string(feature, 'Note')
                
                shift = [0, 0, riboshift]
                utr5_length = self.__get_utr_length(utr5table, feature.id)
                if utr5_length:
                    utr5_length += self.utr5extend
                utr3_length = self.__get_utr_length(utr3table, feature.id) + self.utr3extend
                if utr3_length:
                    utr3_length += self.utr3extend
                full_length = (feature.location.end.position - 
                               feature.location.start.position)

                # note that there is no need to do this here because rfgetcounts
                # handles it
                #if strand == 1:
                #    shift[0] += utr5_length
                #    shift[1] += utr3_length
                #else:
                #    shift[0] += utr3_length
                #    shift[1] += utr5_length

                shift[0] += utr5_length
                shift[1] += utr3_length

                gene_counts, gene_seqs = scgetter.get_counts(chrom, feat_num, shift)
                if gene_counts < 0:
                    continue
               
                coding_length = len(gene_counts) - utr5_length - utr3_length
                # don't need to consider strand because get_counts reverses if -1
                utr5_counts = gene_counts[:utr5_length]
                coding_counts = gene_counts[utr5_length:coding_length + utr5_length]
                utr3_counts = gene_counts[coding_length + utr5_length:]
                
                utr5_seq = gene_seqs[:utr5_length]
                coding_seq = gene_seqs[utr5_length:coding_length + utr5_length]
                utr3_seq = gene_seqs[coding_length + utr5_length:]
                
                utr5_rpkm = self.__calculate_rpkm(utr5_counts)
                coding_rpkm = self.__calculate_rpkm(coding_counts)
                utr3_rpkm = self.__calculate_rpkm(utr3_counts)


                # Should not be in analyze
                # note ?bug in boxcar which reports 3 positions if peak is 
                # island surrounded by 0s --> report last
                record = FeatureRecord(feature.id, alias, chrom, gene_seqs, note)
                #record.set_hidden({"utr5_counts":utr5_counts,
                #                   "coding_counts":coding_counts,
                #                   "utr3_counts":utr3_counts})
                record.set_addl({"utr5_rpkm":[utr5_rpkm],
                                 "coding_rpkm":[coding_rpkm], 
                                 "utr3_rpkm":[utr3_rpkm],
                                 "utr5_length":[utr5_length],
                                 "coding_length":[coding_length],
                                 "utr3_length":[utr3_length],
                                 "utr5_seq":[utr5_seq],
                                 "coding_seq":[coding_seq],
                                 "utr3_seq":[utr3_seq]})
                holder.add_record(record)

        self.holder = holder

    def write(self, out, headers=None):
        assert self.holder
        self.holder.write_to_csv(open(out, 'w'), headers) 
        self.holder.close_csv()

def parse_rfq_config(config):
    configd = {}
    for line in open(config):
        l = line.strip().split(': ')
        if l[1] == "":
            continue
        configd[l[0]] = l[1]
    configd['cleancodingGFF'] = True if configd['cleancodingGFF'] == "True" else False
    configd['checkoverlap'] = True if configd['checkoverlap'] == "True" else False
    configd['riboshift'] = int(configd['riboshift'])
    configd['utr3ext'] = int(configd['utr3ext'])
    configd['utr5ext'] = int(configd['utr5ext'])
    return configd    

def quant_workflow(config):
    
    configd = parse_rfq_config(config)
    
    countsp = rfh.wigtocounts(configd['countsfilestring']+"_plus.wig")
    countsm = rfh.wigtocounts(configd['countsfilestring']+"_minus.wig")
    counts = {1:countsp, -1:countsm}
    
    # parsing gff
    print "parsing gff..."
    coding = rfh.makeGFFlist(configd['codingGFF'])
    if configd['cleancodingGFF']:
        coding = rfh.clean_feature_dict(coding)
    utr5 = rfh.parse_GFF(configd['utr5GFF']) if configd['utr5GFF'] else {}
    utr3 = rfh.parse_GFF(configd['utr3GFF']) if configd['utr3GFF'] else {}

    fq = FeatureQuantifier(counts, configd['riboshift'], coding, utr5, utr3,
                           configd['utr5ext'], configd['utr3ext'])
    fq.quantify()
    out_headers = ["utr5_rpkm", "coding_rpkm", "utr3_rpkm", "utr5_length", 
                   "coding_length", "utr3_length", "utr5_seq", "coding_seq", "utr3_seq"]
    fq.write(configd['outfile']+'.csv', out_headers)

def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument('codingGFF', help = 'coding GFF')
    parser.add_argument('countsfilestring', help = 'input counts string')
    parser.add_argument('--riboshift', default = 13, help = 'ribosome shift')
    parser.add_argument('--cleancodingGFF', default = True, help = 'clean coding GFF?')
    parser.add_argument('--checkoverlap', default = True, help = 'discard if overlap')
    parser.add_argument('--utr5GFF', help = 'utr5 GFF')
    parser.add_argument('--utr3GFF', help = 'utr3 GFF')
    parser.add_argument('--utr5ext', default = 0, 
                        help = "no. of bases to extend 5' UTR by")
    parser.add_argument('--utr3ext', default = 0, 
                        help = "no. of bases to extend 3' UTR by")
    parser.add_argument('--outfile', default="rf_quant.csv", help = "name of csv output file")

    args = parser.parse_args()

    # reading counts
    print "reading counts..."
    #countsp = rfh.readcountsf(args.countsfilestring+"_plus_")
    #countsm = rfh.readcountsf(args.countsfilestring+"_minus_")
    countsp = rfh.wigtocounts(args.countsfilestring+"_plus.wig")
    countsm = rfh.wigtocounts(args.countsfilestring+"_minus.wig")
    counts = {1:countsp, -1:countsm}
    
    # parsing gffs
    print "parsing gff..."
    coding = rfh.clean_feature_dict(rfh.makeGFFlist(args.codingGFF))
    utr5 = rfh.parse_GFF(args.utr5GFF) if args.utr5GFF else {}
    utr3 = rfh.parse_GFF(args.utr3GFF) if args.utr3GFF else {}

    fq = FeatureQuantifier(counts, int(args.riboshift), coding, utr5, utr3, 
                           int(args.utr5ext), int(args.utr3ext))
    fq.quantify()
    out_headers = ["utr5_rpkm", "coding_rpkm", "utr3_rpkm", "utr5_length", 
                   "coding_length", "utr3_length", "utr5_seq", "coding_seq", "utr3_seq"]
    fq.write(args.outfile, out_headers)

#    # test filtering methods
#    filter_fq = fq.holder.filter_by_featids(["YJL082W", "YBR160W"])
#    filter_fq.write_to_csv(open("rf_quant_filter.csv", 'w'), out_headers)
#    filter_fq.close_csv()
#    filter_fq2 = fq.holder.filter_by_addl("coding_rpkm", 1, 0)
#    filter_fq2.write_to_csv(open("rf_quant_filter2.csv", 'w'), out_headers)
#    filter_fq2.close_csv()
#    filter_fq2.write_to_csv(open("rf_quant_filter3.csv", 'w'),
#                            ["utr5_rpkm", "coding_rpkm", "utr3_rpkm"])
#    filter_fq2.close_csv()

if __name__ == '__main__':
    main()
