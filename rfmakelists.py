import argparse
import rfhelper as rfh
from BCBio import GFF
from featrecord import FeatureRecordHolder
from featrecord import FeatureRecord
from rfposavg import PositionAverager

def make_standard_list(coding):
    new_holder = FeatureRecordHolder(addl_headers = ["coding_start", "coding_end"])
    for chrom in coding:
        for feature in coding[chrom].features:
            new_record = FeatureRecord(feature.id, "<alias>", chrom, "<seq>", "<note>", 
                         {"coding_start":[0], "coding_end":[feature.location.end.position -
                                              feature.location.start.position]})
            new_holder.add_record(new_record)
    return new_holder

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("codingGFF", help = 'coding GFF')
    parser.add_argument("countsfilestring", help = 'input counts string')
    parser.add_argument('--riboshift', default = 13, help = 'ribosome shift')
    parser.add_argument('--utr5GFF', default = {}, help = 'utr5GFF')
    parser.add_argument('--utr3GFF', default = {}, help = 'utr3GFF')
    parser.add_argument('--coding5ext', default = 0)
    parser.add_argument('--coding3ext', default = 0)
    parser.add_argument('--window5', default = 100)
    parser.add_argument('--window3', default = 100)
    parser.add_argument('--weighted', default = False)

    args = parser.parse_args()
    coding = rfh.clean_feature_dict(rfh.makeGFFlist(args.codingGFF))
    if args.utr5GFF:
        utr5 = rfh.parse_GFF(args.utr5GFF)
    else:
        utr5 = {}
    if args.utr3GFF:
        utr3 = rfh.parse_GFF(args.utr3GFF)
    else:
        utr3 = {}
    standard_holder = make_standard_list(coding)
    
    countsp = rfh.wigtocounts(args.countsfilestring+"_plus.wig")
    countsm = rfh.wigtocounts(args.countsfilestring+"_minus.wig")
    counts = {1:countsp, -1:countsm}

    pa = PositionAverager(standard_holder, 0, 0, int(args.coding5ext), int(args.coding3ext))
    pa.get_counts(counts, int(args.riboshift), coding, utr5, utr3, True)
    pa.plot_position_average('coding_start', 'coding', 
                             [int(args.window5), int(args.window3)], True)
    pa.plot_position_average('coding_end', 'coding', 
                             [int(args.window5), int(args.window3)], True)
             
if __name__ == '__main__':
    main()
