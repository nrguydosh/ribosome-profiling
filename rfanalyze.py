import argparse
import rfhelper as rfh
from rfgetcounts import SeqCountGetter
from featrecord import FeatureRecord
from featrecord import FeatureRecordHolder
import matplotlib.pyplot as plt
import operator

class ReadingFrameAnalyzer(object):
    """ ReadingFrameAnalyzer analyzes the reading frame. It needs a coding counts
        dictionary, and a coding dictionary, as well as optionally, a 5' utr and 3' utr 
        dictionary. It also takes a ribosome_shift and utr3_extend. It's main method is 
        analyze, which runs the main loop which calculates the reading frame for each
        feature and the overall reading frame. It saves this information in a 
        FeatureRecordHolder. It then plots a histogram of the percentage in the main 
        reading frame for both coding and the 3' utr by default. The write method
        calls the FeatureRecordHolder's write method and adds a comment about the
        overall reading frame. For e.g.

        rfa = ReadingFrameAnalyzer(counts, int(args.riboshift), int(args.utr3extend),
                                   coding, utr5, utr3)
        rfa.analyze()
        rfa.write(outfilename)
        rfa.plot()

    """
    def __init__(self, counts, ribosome_shift, coding, check_overlap=True, utr5=None, utr3=None, utr3_extend=0):
        self.coding = coding
        self.utr5 = {} if not utr5 else utr5
        self.utr3 = {} if not utr3 else utr3
        self.counts = counts
        self.ribosome_shift = ribosome_shift
        self.utr3_extend = utr3_extend

        self.holder_coding_all = None
        self.holder_utr3_all = None
        self.frame_coding_all = None
        self.frame_utr3_all = None

        self.check_overlap = check_overlap

    def __frame_count(self, counts):
        """ Does the frame counting. 
        """
        frame = 3*[0.0]
        for i in range(len(counts)):
            frame[i % 3] += counts[i]
        return frame
    
    def __report_frame_all(self, frame_all):
        """ Quick report for frame_all
        """
        total = sum(frame_all)
        if total == 0:
            output = [0, 0, 0]
        else:
            output = [float(c)/total for c in frame_all]
        print "Fraction of reads mapping to each frame: {0:.4f}, {1:.4f}, {2:.4f}".format(
               output[0], output[1], output[2])
        print "Total reads checked: ", total
    
    def analyze(self):

        coding = self.coding
        utr5 = self.utr5
        utr3 = self.utr3
        counts = self.counts
        riboshift = self.ribosome_shift
        utr3extend = self.utr3_extend
        GFFlist = [coding, utr5, utr3]

        print "for each feature..."
        frame_coding_all = 3*[0.0]
        frame_utr3_all = 3*[0.0]

        addl_headers = ["frame1", "frame2", "frame3"]
        holder_coding_all = FeatureRecordHolder(addl_headers)
        holder_utr3_all = FeatureRecordHolder(addl_headers)

        scgetter = SeqCountGetter(self.counts, self.coding, self.utr5, self.utr3, self.check_overlap)

        for chrom in coding:

            feat_num = -1

            for feature in coding[chrom].features:
                
                feat_num += 1 # start at feat_num = 0

                # get some information about the feature for the csv
                alias = rfh.qualifier_to_string(feature, 'Alias')
                note = rfh.qualifier_to_string(feature, 'Note')
                
                # compute frame mapping for the coding region 
                shift = [0, 0, riboshift]
                codingcounts, codingseq = scgetter.get_counts(chrom, feat_num, shift)
                if codingcounts < 0:
                    continue
                codinglength = len(codingcounts)
                frame_coding = self.__frame_count(codingcounts[24:-15]) 
                                                    # ignore first 24bp, 
                                                    # ignore last 15bp
                frame_coding_all = [frame_coding[i] + frame_coding_all[i] 
                                    for i in range(3)]
                record_coding = FeatureRecord(feature.id, alias, chrom, 
                                             codingseq, note)
                record_addl = {}
                for i in range(len(addl_headers)):
                    record_addl[addl_headers[i]] = [frame_coding[i]]
                record_coding.set_addl(record_addl)
                holder_coding_all.add_record(record_coding) 

                # compute frame mapping for the utr3 region
                if utr3.has_key(feature.id):
                    utr3length = utr3[feature.id][1] - utr3[feature.id][0]
                    utr3shift = [0, utr3length + utr3extend, riboshift]
                else:
                    utr3length = 0
               
                if utr3length > 0: # but only bother if there is in fact a utr3
                    allcounts, allseq = scgetter.get_counts(chrom, feat_num, utr3shift)
                    if allcounts < 0:
                        continue
                    utr3counts = allcounts[codinglength:] # everything after 3'
                    frame_utr3 = self.__frame_count(utr3counts[9:]) # ignore first 9bp
                    frame_utr3_all = [frame_utr3[i] + frame_utr3_all[i] for i in range(3)] 
                    record_utr3 = FeatureRecord(feature.id, alias, chrom, note=note)
                    record_addl = {}
                    for i in range(len(addl_headers)):
                        record_addl[addl_headers[i]] = [frame_utr3[i]]
                    record_utr3.set_addl(record_addl)
                    holder_utr3_all.add_record(record_utr3)

                # note that you CANNOT call givegene for both coding and utr3 together
                # because the variable shift determines whether or not there's overlap

        self.__report_frame_all(frame_coding_all)
        self.__report_frame_all(frame_utr3_all)

        self.holder_coding_all = holder_coding_all
        self.holder_utr3_all = holder_utr3_all
        self.frame_coding_all = frame_coding_all
        self.frame_utr3_all = frame_utr3_all

    def write(self, out):
        coding_out = out + "_coding.csv"
        utr3_out = out + "_utr3.csv"

        self.holder_coding_all.write_to_csv(open(coding_out, 'w'))
        self.holder_coding_all.add_comment_values("Summary",
                                                  {'frame1':self.frame_coding_all[0],
                                                   'frame2':self.frame_coding_all[1],
                                                   'frame3':self.frame_coding_all[2]})
        self.holder_coding_all.close_csv()
        
        self.holder_utr3_all.write_to_csv(open(utr3_out, 'w'))
        self.holder_utr3_all.add_comment_values("Summary",
                                                {'frame1':self.frame_utr3_all[0],
                                                 'frame2':self.frame_utr3_all[1],
                                                 'frame3':self.frame_utr3_all[2]})
        self.holder_utr3_all.close_csv()

    def plot(self, coding_co=5, utr3_co=5, coding_bin=100, utr3_bin=100, title=None):
        rfp = ReadingFrameStats() 
        rfp.add_data_from_holder('coding', self.holder_coding_all)
        rfp.add_data_from_holder('utr3', self.holder_utr3_all)
        
        data_coding, mframe_coding = rfp.get_data('coding')
        data_utr3, mframe_utr3 = rfp.get_data('utr3')
        
        f, axarr = plt.subplots(2)
        hist_coding = [data_coding[fid][mframe_coding] for fid in data_coding.keys()
                       if data_coding[fid]['totalraw'] > coding_co]
        if hist_coding != []:
            axarr[0].hist(hist_coding, coding_bin)
        axarr[0].set_ylabel('frequency')
        axarr[0].set_xlabel('% mapping to main frame for coding ' +  
                            'with {0!s}nt cutoff'.format(coding_co))
        hist_utr3 = [data_utr3[fid][mframe_utr3] for fid in data_utr3.keys()
                     if data_utr3[fid]['totalraw'] > utr3_co]
        if hist_utr3 != []:
            axarr[1].hist(hist_utr3, utr3_bin)
        axarr[1].set_ylabel('frequency')
        axarr[1].set_xlabel('% mapping to main frame for utr3 ' + 
                            'with {0!s}nt cutoff'.format(utr3_co))
        plt.subplots_adjust(top=0.9,wspace=0.2,hspace=0.2)
        if title:
            plt.suptitle(title)
        plt.show()
 
class ReadingFrameStats(object):
    """ ReadingFrameStats calculates some statistics about the reading frame, 
        i.e. the percentage mapped to each frame for each gene, the total raw
        reads mapped to each frame, and the main frame. 
    """

    def __init__(self):
        self.rfdata = {}
        self.mframes = {}

    def add_data_from_holder(self, holder_name, holder):
        assert holder_name not in self.rfdata.keys()
        assert holder_name not in self.mframes.keys()
        data = {}
        for record in holder.get_record_list():
            data[record.fid] = {}
            data[record.fid]['totalraw'] = 0
            for frame in ['frame1', 'frame2', 'frame3']:
                data[record.fid][frame] = record.addl[frame][0]
                # specific to reading frame analysis, expecting only one line
                data[record.fid]['totalraw'] += data[record.fid][frame]
            overallframe = {'pframe1':0, 'pframe2':0, 'pframe3':0}
            for frame in ['frame1', 'frame2', 'frame3']:
                if data[record.fid]['totalraw'] != 0:
                    data[record.fid]['p'+frame] = (data[record.fid][frame]/
                                                   data[record.fid]['totalraw'])
                else:
                    data[record.fid]['p'+frame] = 0
                overallframe['p'+frame] = data[record.fid]['p'+frame]
        self.rfdata[holder_name] = data
        self.mframes[holder_name] =  max(overallframe.iteritems(), 
                                         key=operator.itemgetter(1))[0]

    def get_data(self, holder_name):
        return (self.rfdata[holder_name], self.mframes[holder_name])

def parse_rfa_config(config):
    configd = {}
    for line in open(config):
        l = line.strip().split(': ')
        if l[1] == "":
            continue
        configd[l[0]] = l[1]
    configd['cleancodingGFF'] = True if configd['cleancodingGFF'] == "True" else False
    configd['checkoverlap'] = True if configd['checkoverlap'] == "True" else False
    configd['riboshift'] = int(configd['riboshift'])
    configd['utr3extend'] = int(configd['utr3extend'])
    configd['coding_hist_cutoff'] = int(configd['coding_hist_cutoff'])
    configd['utr3_hist_cutoff'] = int(configd['utr3_hist_cutoff'])
    configd['coding_hist_bin'] = int(configd['coding_hist_bin'])
    configd['utr3_hist_bin'] = int(configd['utr3_hist_bin'])
    return configd    

def rfanalyze_workflow(config):

    configd = parse_rfa_config(config)

    countsp = rfh.wigtocounts(configd['countsfilestring']+"_plus.wig")
    countsm = rfh.wigtocounts(configd['countsfilestring']+"_minus.wig")
    counts = {1:countsp, -1:countsm}
    
    # parsing gff
    print "parsing gff..."
    coding = rfh.makeGFFlist(configd['codingGFF'])
    if configd['cleancodingGFF']:
        coding = rfh.clean_feature_dict(coding)
    if configd['utr5GFF']:
        utr5 = rfh.parse_GFF(configd['utr5GFF'])
    else:
        utr5 = {}
    if configd['utr3GFF']:
        utr3 = rfh.parse_GFF(configd['utr3GFF'])
    else:
        utr3 = {}
    GFFlist = (coding, utr5, utr3)
   
    # Should work with empty utr5 and utr3 dictionaries
    # but can also call ReadingFrameAnalyzer without utr5 and utr3
    # which are optional arguments
    rfa = ReadingFrameAnalyzer(counts, configd['riboshift'], coding,
                               configd['checkoverlap'], utr5, utr3, 
                               configd['utr3extend'])
    rfa.analyze()
    if configd['outfile']:
        rfa.write(configd['outfile'])
    else:
        rfa.write(configd['countsfilestring'])
    rfa.plot(configd['coding_hist_cutoff'], configd['utr3_hist_cutoff'], 
             configd['coding_hist_bin'], configd['utr3_hist_bin'], 
             configd['title'])

def plot_workflow(rfa_csv, co=5, bins=100):

    name = rfa_csv.split('.csv')[0].split('/')[-1]

    holder = FeatureRecordHolder(["frame1", "frame2", "frame3"])
    holder.read_csv(rfa_csv)

    rfp = ReadingFrameStats()
    rfp.add_data_from_holder(name, holder)
    data, mframe = rfp.get_data(name)

    hist_coding = [data[fid][mframe] for fid in data.keys()
                   if data[fid]['totalraw'] > co]
    if hist_coding == []:
        print "No features above cutoff", co
        return
    f = plt.figure()
    ax = f.add_subplot(111)
    ax.hist(hist_coding, bins)
    ax.set_ylabel('frequency')
    ax.set_xlabel('% mapping to main frame for ' + name + 
                  ' with {0!s}nt cutoff'.format(co))
    plt.suptitle(name)
    plt.show()

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('codingGFF', help = 'coding GFF')
    parser.add_argument('countsfilestring', help = 'input counts string')
    parser.add_argument('--riboshift', default = 13, help = 'ribosome shift')
    parser.add_argument('--cleancodingGFF', default = True, help = 'clean coding GFF?')
    parser.add_argument('--checkoverlap', default = True, help = 'discard if overlap')
    parser.add_argument('--utr5GFF', help = 'utr5 GFF')
    parser.add_argument('--utr3GFF', help = 'utr3 GFF')
    parser.add_argument('--utr3extend', default = 0, help = 'utr3 extend')
    parser.add_argument('--out', help = 'output file string, otherwise ' + 
                        'countsfilestring used')
    parser.add_argument

    args = parser.parse_args()

    # reading counts
    print "reading counts..."
    countsp = rfh.wigtocounts(args.countsfilestring+"_plus.wig")
    countsm = rfh.wigtocounts(args.countsfilestring+"_minus.wig")
    counts = {1:countsp, -1:countsm}
    
    # parsing gff
    print "parsing gff..."
    coding = rfh.makeGFFlist(args.codingGFF)
    if args.cleancodingGFF:
        coding = rfh.clean_feature_dict(coding)
    if args.utr5GFF:
        utr5 = rfh.parse_GFF(args.utr5GFF)
    else:
        utr5 = {}
    if args.utr3GFF:
        utr3 = rfh.parse_GFF(args.utr3GFF)
    else:
        utr3 = {}
    GFFlist = (coding, utr5, utr3)
   
    # Should work with empty utr5 and utr3 dictionaries
    # but can also call ReadingFrameAnalyzer without utr5 and utr3
    # which are optional arguments
    rfa = ReadingFrameAnalyzer(counts, int(args.riboshift), coding,
                               args.checkoverlap, utr5, utr3, int(args.utr3extend))
    rfa.analyze()

    if args.out:
        rfa.write(args.out)
    else:
        rfa.write(args.countsfilestring)

    rfa.plot(coding_co=50, title=args.countsfilestring)

if __name__ == '__main__':
    main()
