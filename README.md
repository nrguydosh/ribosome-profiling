Ribosome Profiling
==================
Some (hopefully) handy code for analyzing data for ribosome profiling.

Features to add:
------------------
- Intron quantification
- Construct specific examples to make sure everything works! And because examples
  are easier to follow and would make all this code more useable
- Also add a testing suite
- Implement FeatureRecord with a database instead (or mimic a database. Slower access
  but less problematic in the long run...)
- Procedural versions of all programs (Already available, need to clean up and add to repo)

Update: 13/05/07:
----------------------
- Bug in extend3, extend5 because extending when no utr3' and utr5' is annotated
- In rfquant, do 3' UTR peak finding elsewhere, report length of utr and coding regions,
  as well as utr and coding sequences (seq in -2 column is the 5'utr + coding + 3'utr 
  sequence)
- rfmakelists would contain functions that make lists and that would be called by
  other functions or external wrappers
- rfposavg now does utr5ext, utr3ext, coding5ext, coding3ext, window5, window3
- rfposavg: boolean switch for equal/unequal weights
- rfposavg: Need to have a boolean to decide whether or not to ignore utrs: implemented, 
  need to check if wokring
- Wrapper scripts!!!! / config file 
    - rfanalyze from wig
    - plot rfanalyze from csv
    - rfquant from wig
    - rfposavg from csv

Update: 13/05/04:
----------------------
- Squashed bugs in rfquant and rfanalyze: tested and appears to be be working! 
    - minor mix up in creating attr dictionary
    - shift shouldn't take into account strand because rfgetcounts will deal with that
    - multiply by 1000 to get rpkm
    - handle empty when plotting histogram in rfanalyze
- Also matches sample output from Nick
- Changed rfanalyze, rfquant, rfposavg, to take optional arguments in main scripts
  but probably need greater flexibility
  (temporary solution, need wrapper scripts, or a config file)
- Modified rfposavg and rfquant to do utr5extend and utr3extend: NEEDS TESTING
- Modified get utr3 peaks to return the last position: Still matches sample output
    - Note that sometimes I find peaks when there isn't one in sample output
    - Also filtering is probably different because I'm reporting more genes, maybe
      because overlap is FALSE for rfquant at the moment

Update: 13/04/22:
------------------
- First attempt at handling duplicates more elegantly
- FeatureRecord now has a remove duplicates method that removes duplicates from the addl
  dictionary, given headers that constrain whether to decide if entries are unique
- This method is used when adding a record in FeatureRecordHolder
- It therefore handles duplicates when reading in a csv
- Remove-duplicates is also useful for filtering by addl headers, whereby after
  filtering there is a need to check if there are duplicates. It handles building
  a new record in a slightly different way that should also be valid
- Basically, the duplication handling is now within the class, whereas the burden of
  making sure that the entries match up is placed on the user, which makes more sense
  since those are arbitrary whereas duplication handling should be part of the
  functionality since it is needed for reading in csvs and also filtering (though
  duplication should not happen in csvs)
- The class is not entirely friendly in the sense that there are some attributes
  that you would not expect to be a list are lists for e.g. each attr[key]. It
  would be nice to automate this.
  for all entries (this is probably usually the case)
- Next step: Make sure rfquant is compatible with the changes
- Seems to work? Ran the usual rfanalyze as well as testfeatrecord, and in rfquant 
  Still needs more exhaustive testing. 
- Changed seq handling so that seq is whatever it was set to the first time around. This
  means that records can't have variable seqs (if seqs vary for the same feature it
  should be set in the addl dictionary instead) (Burden is on user)

Some boring history:
---------------------
- Port to github
- Plotting for rfanalyze in matplotlib (Check against R)...working
- FeatureRecordHolder should read a csv...working  
- Test multiple line writing in featrecord and FeatureRecordHolder should read a csv by
  checking re-writing csv is the same...working
- Handle if utr3 and utr5 dictionaries are empty...working (see note in rfanalyze)
- Test plotting with reading in the csv
- Change seq in FeatureRecord to a list
- Implement rfquant
- Filter by attr
- Filter by addl
- Do filtering in featrecord instead
- Read wig instead of binary 
- Note that right now reading/writing need to try to force coercion to float
  otherwise comparing might fail after reading in csv
