import argparse
import rfanalyze as rfa

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('rfa_csv', help = "CSV to read containing frame information")
    parser.add_argument('--cutoff', default=50, help="Cutoff for total raw reads " + 
                                                     "(Default:50)")
    parser.add_argument('--bins', default=100, help="Number of bins for histogram " + 
                                                     "(Default: 100)")
    args = parser.parse_args()

    rfa.plot_workflow(args.rfa_csv, int(args.cutoff), int(args.bins))

if __name__ == '__main__':
    main()
    
